import { StyleSheet } from "react-native";

export const profileStyle = StyleSheet.create({
  container: {
    height: "100%",
    backgroundColor: "#E8E8DC",
    paddingTop: 28
  }
});