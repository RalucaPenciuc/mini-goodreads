import { StyleSheet } from "react-native";

export const dashboardStyle = StyleSheet.create({
  container: {
    height: "100%",
    backgroundColor: "#E8E8DC",
    paddingTop: 28
  },
  list: {
    width: "100%"
  }
});
