import { StyleSheet } from "react-native";

export const sendMailStyle = StyleSheet.create({
  container: {
    height: "100%",
    backgroundColor: "#E8E8DC",
    paddingTop: 28
  },
  formContainer: {
    height: "100%",
    display: "flex",
    alignItems: "center"
  },
  title: {
    width: "80%",
    fontWeight: "bold",
    fontSize: 20,
    textAlign: "center",
    marginTop: 10,
    marginBottom: 20
  },
  formFieldContainer: {
    width: "80%",
    marginBottom: 25
  },
  emailField: {
    borderWidth: 1,
    borderColor: "black",
    padding: 3
  },
  messageField: {
    height: 100,
    borderWidth: 1,
    borderColor: "black",
    padding: 3
  },
  label: {
    fontWeight: "bold",
    fontSize: 17
  },
  buttonContainer: {
    width: "60%"
  }
});