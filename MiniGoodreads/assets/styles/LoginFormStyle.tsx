import { StyleSheet } from "react-native";

export const loginFormStyles = StyleSheet.create({
  page: {
    height: "100%",
    backgroundColor: "#E8E8DC",
    display: "flex",
    alignItems: "center"
  },
  title: {
    fontSize: 40,
    color: "#382110",
    marginTop: 100,
    marginBottom: 50
  },
  form: {
    display: "flex",
    justifyContent: "space-around",
    alignItems: "center",
    width: "90%"
  },
  input: {
    margin: 10,
    backgroundColor: "white",
    borderColor: "gray",
    borderWidth: 0.5,
    width: "100%"
  },
  button: {
    width: 70,
    backgroundColor: "#382110",
    marginTop: 15
  },
  error: {
    color: "red"
  }
});
