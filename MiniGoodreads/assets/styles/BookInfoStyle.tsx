import { StyleSheet } from "react-native";

export const bookInfoStyle = StyleSheet.create({
  container: {
    height: "100%",
    backgroundColor: "#E8E8DC",
    paddingTop: 28
  },
  screenContent: {
    height: "100%",
    padding: 15
  },
  editButtonsContainer: {
    display: "flex",
    flexDirection: "row",
    alignSelf: "flex-end",
    flexGrow: 0.5
  },
  formContainer: {
    flexGrow: 2
  },
  saveButtonsContainer: {
    display: "flex",
    flexDirection: "row",
    alignSelf: "flex-end",
    flexGrow: 0.5
  },
  formFieldContainer: {
    marginBottom: 25
  },
  formField: {
    borderWidth: 1,
    borderColor: "black",
    padding: 3
  },
  label: {
    fontWeight: "bold",
    fontSize: 17
  },
  dialogContainer: {
    backgroundColor: "#E8E8DC"
  },
  dialogTitle: {
    backgroundColor: "#382110",
    height: 50
  },
  dialogTitleText: {
    color: "white"
  },
  dialogFooter: {
    height: 50
  },
  dialogCancelButtonText: {
    color: "#FF7F7F"
  },
  dialogSaveButtonText: {
    color: "#382110"
  }
});