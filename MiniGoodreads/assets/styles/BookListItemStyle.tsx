import { StyleSheet } from "react-native";

export const bookListItemStyle = StyleSheet.create({
  container: {
    display: "flex",
    flexDirection: "row",
    marginTop: 5,
    marginBottom: 2,
    marginLeft: 3,
    marginRight: 3,
    borderWidth: 2,
    borderColor: "#D8D8D8"
  },
  readBook: {
    backgroundColor: "#7FFF7F",
    width: 10
  },
  newBook: {
    backgroundColor: "#FF7F7F",
    width: 10
  },
  contentContainer: {
    width: 300,
    paddingLeft: 15,
    paddingTop: 10,
    paddingBottom: 10
  },
  contentTitle: {
    fontSize: 18,
    fontWeight: "bold"
  },
  buttonsContainer: {
    height: "50%",
    display: "flex",
    flexDirection: "row",
    alignSelf: "center"
  }
});
