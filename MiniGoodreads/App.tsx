import React, { Component } from "react";
import { Provider } from "react-redux";
import configureStore from "./src/redux/store/configure-store";
import AppContainer from "./src/routes/AppNavigator";
import { PersistGate } from "redux-persist/integration/react";
import { ActivityIndicator, View } from "react-native";

const { persistor, store } = configureStore();

export default class App extends Component {
  renderLoading = () => {
    return (
      <View>
        <ActivityIndicator
          animating={true}
          color="#382110"
          size={"large"}
          style={{
            marginTop: 300
          }}
        />
      </View>
    );
  };

  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor} loading={this.renderLoading()}>
          <AppContainer />
        </PersistGate>
      </Provider>
    );
  }
}
