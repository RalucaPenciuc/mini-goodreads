import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import LoginFormComponent from "../components/LoginFormComponent";
import DashboardComponent from "../components/DashboardComponent";
import BookInfoComponent from "../components/BookInfoComponent";
import AddBookFormComponent from "../components/AddBookFormComponent";
import ProfileComponent from "../components/ProfileComponent";
import SendMailComponent from "../components/SendMailComponent";

export const AppNavigator = createStackNavigator(
  {
    // RouteConfigs param
    LoginPage: {
      screen: LoginFormComponent
    },
    DashboardPage: {
      screen: DashboardComponent
    },
    BookInfoPage: {
      screen: BookInfoComponent
    },
    AddBookPage: {
      screen: AddBookFormComponent
    },
    ProfilePage: {
      screen: ProfileComponent
    },
    SendMailPage: {
      screen: SendMailComponent
    }
  },
  {
    // StackNavigatorParam
    initialRouteName: "LoginPage",
    headerMode: "none"
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;
