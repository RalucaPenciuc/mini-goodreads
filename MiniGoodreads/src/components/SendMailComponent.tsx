import React, { Component } from "react";
import { View, Text, Alert } from "react-native";
import { Toolbar, Button } from "react-native-material-ui";
import { NavigationStackProp } from "react-navigation-stack";
import { NavigationState } from "react-navigation";
import { sendMailStyle } from "../../assets/styles/SendMailStyle";
import { TextInput } from "react-native-gesture-handler";
import { sendEmail } from "../services/HttpService";

interface IProps {
  navigation: NavigationStackProp<NavigationState>;
}

interface IState {
  emailAddress: string;
  message: string;
}

export default class SendMailComponent extends Component<IProps, IState> {
  constructor(props: any) {
    super(props);
    this.state = {
      emailAddress: "",
      message: ""
    };
  }

  navigateBack = (): void => {
    const { navigation } = this.props;
    navigation.navigate("BookInfoPage");
  };

  handleChangeEmail = (text: string): void => {
    this.setState({
      emailAddress: text
    });
  };

  handleChangeMessage = (text: string): void => {
    this.setState({
      message: text
    });
  };

  handleSendButton = (): void => {
    sendEmail(
      this.state.emailAddress,
      "BetterReads Recommendation",
      this.state.message
    ).then(() => {
      console.log("Our email successful provided to device mail ");
    });
  };

  render() {
    return (
      <View style={sendMailStyle.container}>
        <Toolbar
          style={{
            container: {
              backgroundColor: "#382110"
            }
          }}
          leftElement="arrow-back"
          centerElement="BetterReads"
          onLeftElementPress={() => this.navigateBack()}
        />
        <View style={sendMailStyle.formContainer}>
          <Text style={sendMailStyle.title}>Send recommendation</Text>
          <View style={sendMailStyle.formFieldContainer}>
            <Text style={sendMailStyle.label}>To: </Text>
            <TextInput
              style={sendMailStyle.emailField}
              value={this.state.emailAddress}
              onChangeText={(text: string) => this.handleChangeEmail(text)}
            />
          </View>
          <View style={sendMailStyle.formFieldContainer}>
            <Text style={sendMailStyle.label}>Message: </Text>
            <TextInput
              style={sendMailStyle.messageField}
              value={this.state.message}
              onChangeText={(text: string) => this.handleChangeMessage(text)}
            />
          </View>
          <View style={sendMailStyle.buttonContainer}>
            <Button
              text="Send book recommendation"
              style={{
                container: {
                  borderColor: "#382110",
                  borderWidth: 1
                },
                text: {
                  color: "#382110",
                  textAlign: "center"
                }
              }}
              onPress={() => this.handleSendButton()}
            />
          </View>
        </View>
      </View>
    );
  }
}
