import React, { Component } from "react";
import { View, Text, TextInput, CheckBox } from "react-native";
import { bookInfoStyle } from "../../assets/styles/BookInfoStyle";
import { Toolbar, Button } from "react-native-material-ui";
import Book from "../models/Book";
import { NavigationStackProp } from "react-navigation-stack";
import { NavigationState } from "react-navigation";
import { connect } from "react-redux";
import { addBookBegin } from "../redux/actions/actions";

interface IProps {
  navigation: NavigationStackProp<NavigationState>;
  addBook: Function;
}

interface IState {
  typingBook: Book;
  defaultValues: Book;
}

class AddBookFormComponent extends Component<IProps, IState> {
  constructor(props: any) {
    super(props);
    this.state = {
      typingBook: {
        _id: "",
        title: "",
        author: "",
        read: false
      },
      defaultValues: {
        _id: "",
        title: "",
        author: "",
        read: false
      }
    };
  }

  changeTitleHandler = (text: string): void => {
    this.setState({
      typingBook: {
        _id: this.state.typingBook._id,
        title: text,
        author: this.state.typingBook.author,
        read: this.state.typingBook.read
      }
    });
  };

  changeAuthorHandler = (text: string): void => {
    this.setState({
      typingBook: {
        _id: this.state.typingBook._id,
        title: this.state.typingBook.title,
        author: text,
        read: this.state.typingBook.read
      }
    });
  };

  changeReadHandler = (event: boolean): void => {
    this.setState({
      typingBook: {
        _id: this.state.typingBook._id,
        title: this.state.typingBook.title,
        author: this.state.typingBook.author,
        read: event
      }
    });
  };

  clickCancelButtonHandler = (): void => {
    this.setState({ typingBook: this.state.defaultValues });
    const { navigation } = this.props;
    navigation.navigate("DashboardPage");
  };

  clickSaveButtonHandler = (): void => {
    if (
      !(
        this.state.typingBook.title.length == 0 &&
        this.state.typingBook.author.length == 0 &&
        this.state.typingBook.read == false
      )
    ) {
      this.props.addBook(this.state.typingBook);
      this.setState({ typingBook: this.state.defaultValues });
      const { navigation } = this.props;
      navigation.navigate("DashboardPage");
    } else console.log("CANNOT ADD AN EMPTY BOOK");
  };

  render() {
    return (
      <View style={bookInfoStyle.container}>
        <Toolbar
          style={{
            container: {
              backgroundColor: "#382110"
            }
          }}
          leftElement="arrow-back"
          centerElement="BetterReads"
          onLeftElementPress={() => this.clickCancelButtonHandler()}
        />
        <View style={bookInfoStyle.screenContent}>
          <View style={bookInfoStyle.formContainer}>
            <View style={bookInfoStyle.formFieldContainer}>
              <Text style={bookInfoStyle.label}>Title: </Text>
              <TextInput
                style={bookInfoStyle.formField}
                value={this.state.typingBook.title}
                onChangeText={(text: string) => this.changeTitleHandler(text)}
              />
            </View>
            <View style={bookInfoStyle.formFieldContainer}>
              <Text style={bookInfoStyle.label}>Author: </Text>
              <TextInput
                style={bookInfoStyle.formField}
                value={this.state.typingBook.author}
                onChangeText={(text: string) => this.changeAuthorHandler(text)}
              />
            </View>
            <View style={bookInfoStyle.formFieldContainer}>
              <Text style={bookInfoStyle.label}>I read this book: </Text>
              <CheckBox
                value={this.state.typingBook.read}
                onValueChange={(event: boolean) =>
                  this.changeReadHandler(event)
                }
              />
            </View>
          </View>
          <View style={bookInfoStyle.saveButtonsContainer}>
            <Button
              text="Cancel"
              style={{
                container: {
                  borderColor: "#ff7f7f",
                  borderWidth: 1
                },
                text: {
                  color: "#ff7f7f"
                }
              }}
              onPress={() => this.clickCancelButtonHandler()}
            />
            <Button
              text="Save"
              style={{
                container: {
                  marginLeft: 5,
                  borderColor: "#382110",
                  borderWidth: 1
                },
                text: {
                  color: "#382110"
                }
              }}
              onPress={() => this.clickSaveButtonHandler()}
            />
          </View>
        </View>
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    addBook: (book: Book) => dispatch(addBookBegin(book))
  };
};

export default connect(null, mapDispatchToProps)(AddBookFormComponent);
