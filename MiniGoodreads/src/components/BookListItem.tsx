import React, { PureComponent } from "react";
import { View, Text } from "react-native";
import Book from "../models/Book";
import { bookListItemStyle } from "../../assets/styles/BookListItemStyle";
import { NavigationStackProp } from "react-navigation-stack";
import { NavigationState } from "react-navigation";

interface IProps {
  navigation: NavigationStackProp<NavigationState>;
  content: Book;
}

interface IState {}

export default class BookListItem extends PureComponent<IProps, IState> {
  constructor(props: any) {
    super(props);
    console.ignoredYellowBox = ["Setting a timer"];
  }

  onTouchEvent = () => {
    const { navigation } = this.props;
    navigation.navigate("BookInfoPage", {
      bookDocRef: this.props.content._id,
      bookTitle: this.props.content.title,
      bookAuthor: this.props.content.author,
      bookRead: this.props.content.read
    });
  };

  render() {
    return (
      <View
        style={bookListItemStyle.container}
        onTouchEnd={() => this.onTouchEvent()}
      >
        <View
          style={
            this.props.content.read
              ? bookListItemStyle.readBook
              : bookListItemStyle.newBook
          }
        ></View>
        <View style={bookListItemStyle.contentContainer}>
          <Text style={bookListItemStyle.contentTitle}>
            {this.props.content.title}
          </Text>
          <Text>{this.props.content.author}</Text>
        </View>
      </View>
    );
  }
}
