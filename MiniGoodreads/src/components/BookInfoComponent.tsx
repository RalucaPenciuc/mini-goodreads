import React, { Component } from "react";
import {
  View,
  Text,
  CheckBox,
  NativeSyntheticEvent,
  TextInputChangeEventData
} from "react-native";
import Book from "../models/Book";
import { NavigationStackProp } from "react-navigation-stack";
import { NavigationState } from "react-navigation";
import { Toolbar, Icon, Button } from "react-native-material-ui";
import { bookInfoStyle } from "../../assets/styles/BookInfoStyle";
import { TextInput } from "react-native-gesture-handler";
import Dialog, {
  DialogContent,
  DialogFooter,
  DialogButton,
  DialogTitle,
  SlideAnimation
} from "react-native-popup-dialog";
import { connect } from "react-redux";
import { deleteBookBegin, editBookBegin } from "../redux/actions/actions";

interface IProps {
  navigation: NavigationStackProp<NavigationState>;
  editBook: Function;
  deleteBook: Function;
}

interface IState {
  book: Book;
  typingBook: Book;
  editButtonClicked: boolean;
  deleteButtonClicked: boolean;
}

class BookInfoComponent extends Component<IProps, IState> {
  constructor(props: any) {
    super(props);
    console.ignoredYellowBox = ["Setting a timer"];
    this.state = {
      book: {
        _id: this.props.navigation.getParam("bookDocRef", ""),
        title: this.props.navigation.getParam("bookTitle", ""),
        author: this.props.navigation.getParam("bookAuthor", ""),
        read: this.props.navigation.getParam("bookRead", "")
      },
      typingBook: {
        _id: this.props.navigation.getParam("bookDocRef", ""),
        title: this.props.navigation.getParam("bookTitle", ""),
        author: this.props.navigation.getParam("bookAuthor", ""),
        read: this.props.navigation.getParam("bookRead", "")
      },
      editButtonClicked: false,
      deleteButtonClicked: false
    };
  }

  clickEditButtonHandler = (): void => {
    this.setState({ editButtonClicked: true });
  };

  clickCancelEditButtonHandler = (): void => {
    this.setState({
      typingBook: {
        _id: this.state.book._id,
        title: this.state.book.title,
        author: this.state.book.author,
        read: this.state.book.read
      },
      editButtonClicked: false
    });
  };

  clickSaveEditButtonHandler = (): void => {
    this.props.editBook(this.state.typingBook);
    this.setState({ editButtonClicked: false });
    const { navigation } = this.props;
    navigation.navigate("DashboardPage");
  };

  clickDeleteButtonHandler = (): void => {
    this.setState({ deleteButtonClicked: true });
  };

  clickCancelDeleteButtonHandler = (): void => {
    this.setState({ deleteButtonClicked: false });
  };

  clickConfirmDeleteButtonHandler = (): void => {
    this.props.deleteBook(this.state.book);
    this.setState({ deleteButtonClicked: false });
    const { navigation } = this.props;
    navigation.navigate("DashboardPage");
  };

  clickSendMailButtonHandler = (): void => {
    const { navigation } = this.props;
    navigation.navigate("SendMailPage", {
      bookTitle: this.state.book.title,
      bookAuthor: this.state.book.author
    });
  };

  changeTitleHandler = (text: string): void => {
    this.setState({
      typingBook: {
        _id: this.state.typingBook._id,
        title: text,
        author: this.state.typingBook.author,
        read: this.state.typingBook.read
      }
    });
  };

  changeAuthorHandler = (text: string): void => {
    this.setState({
      typingBook: {
        _id: this.state.typingBook._id,
        title: this.state.typingBook.title,
        author: text,
        read: this.state.typingBook.read
      }
    });
  };

  changeReadHandler = (event: boolean): void => {
    this.setState({
      typingBook: {
        _id: this.state.typingBook._id,
        title: this.state.typingBook.title,
        author: this.state.typingBook.author,
        read: event
      }
    });
  };

  navigateBack = (): void => {
    if (this.state.editButtonClicked) {
      this.setState({
        typingBook: this.state.book,
        editButtonClicked: false
      });
    }
    const { navigation } = this.props;
    navigation.navigate("DashboardPage");
  };

  render() {
    return (
      <View style={bookInfoStyle.container}>
        <Toolbar
          style={{
            container: {
              backgroundColor: "#382110"
            }
          }}
          leftElement="arrow-back"
          centerElement="BetterReads"
          onLeftElementPress={() => this.navigateBack()}
        />
        <View style={bookInfoStyle.screenContent}>
          <Dialog
            dialogStyle={bookInfoStyle.dialogContainer}
            visible={this.state.deleteButtonClicked}
            dialogTitle={
              <DialogTitle
                title="Confirm delete"
                style={bookInfoStyle.dialogTitle}
                textStyle={bookInfoStyle.dialogTitleText}
              />
            }
            footer={
              <DialogFooter style={bookInfoStyle.dialogFooter}>
                <DialogButton
                  textStyle={bookInfoStyle.dialogCancelButtonText}
                  key="cancel-button"
                  text="Cancel"
                  onPress={() => this.clickCancelDeleteButtonHandler()}
                />
                <DialogButton
                  textStyle={bookInfoStyle.dialogSaveButtonText}
                  key="yes-button"
                  text="Yes"
                  onPress={() => this.clickConfirmDeleteButtonHandler()}
                />
              </DialogFooter>
            }
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "top"
              })
            }
            onTouchOutside={() => {
              this.setState({
                deleteButtonClicked: false
              });
            }}
          >
            <DialogContent>
              <Text>Are you sure you want to delete this book?</Text>
            </DialogContent>
          </Dialog>
          <View style={bookInfoStyle.editButtonsContainer}>
            <Button
              text=""
              icon={<Icon name="mail" color="#382110" />}
              onPress={() => this.clickSendMailButtonHandler()}
            />
            <Button
              text=""
              icon={<Icon name="edit" color="#382110" />}
              onPress={() => this.clickEditButtonHandler()}
            />
            <Button
              text=""
              icon={<Icon name="delete" color="#ff7f7f" />}
              onPress={() => this.clickDeleteButtonHandler()}
            />
          </View>
          <View style={bookInfoStyle.formContainer}>
            <View style={bookInfoStyle.formFieldContainer}>
              <Text style={bookInfoStyle.label}>Title: </Text>
              <TextInput
                style={bookInfoStyle.formField}
                editable={this.state.editButtonClicked}
                value={this.state.typingBook.title}
                onChangeText={(text: string) => this.changeTitleHandler(text)}
              />
            </View>
            <View style={bookInfoStyle.formFieldContainer}>
              <Text style={bookInfoStyle.label}>Author: </Text>
              <TextInput
                style={bookInfoStyle.formField}
                editable={this.state.editButtonClicked}
                value={this.state.typingBook.author}
                onChangeText={(text: string) => this.changeAuthorHandler(text)}
              />
            </View>
            <View style={bookInfoStyle.formFieldContainer}>
              <Text style={bookInfoStyle.label}>I read this book: </Text>
              <CheckBox
                disabled={!this.state.editButtonClicked}
                value={this.state.typingBook.read}
                onValueChange={(event: boolean) =>
                  this.changeReadHandler(event)
                }
              />
            </View>
          </View>
          {this.state.editButtonClicked ? (
            <View style={bookInfoStyle.saveButtonsContainer}>
              <Button
                text="Cancel"
                style={{
                  container: {
                    borderColor: "#ff7f7f",
                    borderWidth: 1
                  },
                  text: {
                    color: "#ff7f7f"
                  }
                }}
                onPress={() => this.clickCancelEditButtonHandler()}
              />
              <Button
                text="Save"
                style={{
                  container: {
                    marginLeft: 5,
                    borderColor: "#382110",
                    borderWidth: 1
                  },
                  text: {
                    color: "#382110"
                  }
                }}
                onPress={() => this.clickSaveEditButtonHandler()}
              />
            </View>
          ) : (
            <View style={bookInfoStyle.saveButtonsContainer}>
              <Button
                text="Cancel"
                disabled={true}
                style={{
                  container: {
                    borderColor: "#FFBFBF",
                    borderWidth: 1
                  },
                  text: {
                    color: "#FFBFBF"
                  }
                }}
              />
              <Button
                text="Save"
                disabled={true}
                style={{
                  container: {
                    marginLeft: 5,
                    borderColor: "#CE8854",
                    borderWidth: 1
                  },
                  text: {
                    color: "#CE8854"
                  }
                }}
              />
            </View>
          )}
        </View>
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    editBook: (newBook: Book) => dispatch(editBookBegin(newBook)),
    deleteBook: (book: Book) => dispatch(deleteBookBegin(book))
  };
};

export default connect(null, mapDispatchToProps)(BookInfoComponent);
