import React, { Component } from "react";
import { View, Dimensions } from "react-native";
import { Toolbar } from "react-native-material-ui";
import { profileStyle } from "../../assets/styles/ProfileStyle";
import { NavigationStackProp } from "react-navigation-stack";
import { NavigationState } from "react-navigation";
import { PieChart } from "react-native-chart-kit";
import Book from "../models/Book";
import AppState from "../redux/store/appStore";
import { connect } from "react-redux";
import ChartObject from "../models/ChartObject";
import FadeInView from "./AnimationComponent";

interface IProps {
  navigation: NavigationStackProp<NavigationState>;
  books: Book[];
}

interface IState {}

class ProfileComponent extends Component<IProps, IState> {
  goBack = (): void => {
    const { navigation } = this.props;
    navigation.navigate("DashboardPage");
  };

  getChartData = (): ChartObject[] => {
    let chartData: ChartObject[] = [];
    let noRead: number = 0;
    let noNotRead: number = 0;
    this.props.books.forEach((book: Book) => {
      if (book.read) {
        noRead++;
      } else if (!book.read) {
        noNotRead++;
      }
    });
    let chartObjectRead: ChartObject = {
      name: "Read",
      books: noRead,
      color: "#7FFF7F",
      legendFontColor: "#382110",
      legendFontSize: 15,
      legendFontWeight: "bold"
    };
    let chartObjectNotRead: ChartObject = {
      name: "Not Read",
      books: noNotRead,
      color: "#FF7F7F",
      legendFontColor: "#382110",
      legendFontSize: 15,
      legendFontWeight: "bold"
    };
    chartData.push(chartObjectRead);
    chartData.push(chartObjectNotRead);
    return chartData;
  };

  render() {
    return (
      <View style={profileStyle.container}>
        <Toolbar
          style={{
            container: {
              backgroundColor: "#382110"
            }
          }}
          leftElement="arrow-back"
          centerElement="BetterReads"
          onLeftElementPress={() => this.goBack()}
        />
        <FadeInView>
          <PieChart
            data={this.getChartData()}
            width={Dimensions.get("window").width - 16}
            height={220}
            chartConfig={{
              color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
              labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`
            }}
            accessor="books"
            backgroundColor="transparent"
            paddingLeft="15"
          />
        </FadeInView>
      </View>
    );
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    books: state.books
  };
};

export default connect(mapStateToProps, null)(ProfileComponent);
