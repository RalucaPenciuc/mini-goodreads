import React, { Component } from "react";
import { View, FlatList, BackHandler } from "react-native";
import { connect } from "react-redux";
import AppState from "../redux/store/appStore";
import { dashboardStyle } from "../../assets/styles/DashboardStyle";
import { NavigationStackProp } from "react-navigation-stack";
import { NavigationState } from "react-navigation";
import Book from "../models/Book";
import { fetchBooksBegin, logoutUser } from "../redux/actions/actions";
import BookListItem from "./BookListItem";
import { Toolbar, RightElementPressEvent } from "react-native-material-ui";

interface IProps {
  navigation: NavigationStackProp<NavigationState>;
  error: string;
  loading: boolean;
  books: Book[];
  userLoggedIn: Boolean;
  fetchBooks(): Function;
  logout(): Function;
}

interface IState {}

class DashboardComponent extends Component<IProps, IState> {
  flatListRef: FlatList<Book>;

  constructor(props: any) {
    super(props);
    console.ignoredYellowBox = ["Setting a timer"];
  }

  componentDidMount() {
    this.props.fetchBooks();
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
  }

  handleBackButton() {
    return true;
  }

  goToProfilePage = (): void => {
    const { navigation } = this.props;
    navigation.navigate("ProfilePage");
  };

  handleRightIconTouch = (event: RightElementPressEvent): void => {
    const { navigation } = this.props;
    if (event.action === "add") {
      navigation.navigate("AddBookPage");
    } else if (event.action === "exit-to-app") {
      this.props.logout();
    }
  };

  componentDidUpdate() {
    if (this.props.userLoggedIn == false) {
      const { navigation } = this.props;
      navigation.navigate("LoginPage");
    }
  }

  render() {
    return (
      <View style={dashboardStyle.container}>
        <Toolbar
          style={{
            container: {
              backgroundColor: "#382110"
            }
          }}
          leftElement="person"
          centerElement="BetterReads"
          rightElement={{
            actions: ["add", "exit-to-app"]
          }}
          onLeftElementPress={() => this.goToProfilePage()}
          onRightElementPress={(action: RightElementPressEvent) =>
            this.handleRightIconTouch(action)
          }
        />
        <FlatList
          ref={ref => {
            this.flatListRef = ref;
          }}
          style={dashboardStyle.list}
          data={this.props.books}
          initialNumToRender={7}
          renderItem={({ item }) => (
            <BookListItem content={item} navigation={this.props.navigation} />
          )}
        />
      </View>
    );
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    error: state.error,
    loading: state.loading,
    books: state.books,
    userLoggedIn: state.userLoggedIn
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    fetchBooks: () => dispatch(fetchBooksBegin()),
    logout: () => dispatch(logoutUser())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DashboardComponent);
