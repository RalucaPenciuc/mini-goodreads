import React, { Component } from "react";
import { loginFormStyles } from "../../assets/styles/LoginFormStyle";
import { View, Text, TextInput } from "react-native";
import { Button } from "react-native-elements";
import { connect } from "react-redux";
import User from "../models/User";
import AppState from "../redux/store/appStore";
import { NavigationStackProp } from "react-navigation-stack";
import { NavigationState } from "react-navigation";
import { loginUserCredentialsBegin } from "../redux/actions/actions";

interface LoginFormProps {
  navigation: NavigationStackProp<NavigationState>;
  loading: boolean;
  error: string;
  userLoggedIn: boolean;
  loginUserCredentials: Function;
}

interface LoginFormState {
  user: User;
}

class LoginFormComponent extends Component<LoginFormProps, LoginFormState> {
  constructor(props: any) {
    super(props);
    console.ignoredYellowBox = ["Setting a timer"];
    this.state = {
      user: {
        username: "",
        password: ""
      }
    };
  }

  handleUsernameChange(typedEmail: string): void {
    this.setState({
      user: {
        username: typedEmail,
        password: this.state.user.password
      }
    });
  }

  handlePasswordChange(typedPassword: string): void {
    this.setState({
      user: {
        username: this.state.user.username,
        password: typedPassword
      }
    });
  }

  async handleSubmitForm() {
    this.props.loginUserCredentials(this.state.user);
  }

  componentDidMount() {
    if (this.props.userLoggedIn) {
      const { navigation } = this.props;
      navigation.navigate("DashboardPage");
    }
  }

  componentDidUpdate() {
    if (this.props.userLoggedIn) {
      const { navigation } = this.props;
      navigation.navigate("DashboardPage");
    }
  }

  render() {
    return (
      <View style={loginFormStyles.page}>
        <Text style={loginFormStyles.title}>BetterReads</Text>
        <View style={loginFormStyles.form}>
          <Text style={loginFormStyles.error}>{this.props.error}</Text>
          <TextInput
            style={loginFormStyles.input}
            value={this.state.user.username}
            onChangeText={email => this.handleUsernameChange(email)}
            placeholder={"Email"}
          />
          <TextInput
            style={loginFormStyles.input}
            value={this.state.user.password}
            onChangeText={password => this.handlePasswordChange(password)}
            placeholder={"Password"}
            secureTextEntry={true}
          />
          <Button
            buttonStyle={loginFormStyles.button}
            title={"Login"}
            onPress={this.handleSubmitForm.bind(this)}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    error: state.error,
    loading: state.loading,
    userLoggedIn: state.userLoggedIn
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    loginUserCredentials: (user: User) =>
      dispatch(loginUserCredentialsBegin(user))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginFormComponent);
