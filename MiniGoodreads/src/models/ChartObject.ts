export default interface ChartObject {
  name: string;
  books: number;
  color: string;
  legendFontColor: string;
  legendFontSize: number;
  legendFontWeight: string;
}
