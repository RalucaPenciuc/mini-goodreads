export default interface Book {
  _id?: string;
  title: string;
  author: string;
  read: boolean;
}
