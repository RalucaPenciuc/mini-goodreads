import { takeLeading, takeEvery } from "@redux-saga/core/effects";
import { fetchBooksHandler, loginUserCredentialsHandler, deleteBookHandler, editBookHandler, addBookHandler } from "./Listeners";
import {
  LOGIN_USER_CREDENTIALS_BEGIN,
  FETCH_BOOKS_BEGIN,
  DELETE_BOOK_BEGIN,
  EDIT_BOOK_BEGIN,
  ADD_BOOK_BEGIN
} from "../actions/types";

export function* generalSaga(): IterableIterator<any> {
  yield takeLeading(LOGIN_USER_CREDENTIALS_BEGIN, loginUserCredentialsHandler);
  yield takeLeading(FETCH_BOOKS_BEGIN, fetchBooksHandler);
  yield takeEvery(ADD_BOOK_BEGIN, addBookHandler);
  yield takeEvery(EDIT_BOOK_BEGIN, editBookHandler);
  yield takeEvery(DELETE_BOOK_BEGIN, deleteBookHandler);
}
