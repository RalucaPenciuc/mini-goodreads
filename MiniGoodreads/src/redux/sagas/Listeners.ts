import HttpService from "../../services/HttpService";
import { AppActionType } from "../actions/types";
import IHttpService from "../../services/interfaces/IHttpService";
import { call, put } from "@redux-saga/core/effects";
import {
  loginUserCredentialsSuccess,
  loginUserCredentialsError,
  fetchBooksSuccess,
  fetchBooksError,
  deleteBookSuccess,
  deleteBookError,
  editBookSuccess,
  editBookError,
  addBookSuccess,
  addBookError
} from "../actions/actions";
import User from "../../models/User";
import Book from "../../models/Book";
import { AsyncStorage } from "react-native";

const httpService: IHttpService = HttpService.getInstance();

export function* loginUserCredentialsHandler(action: AppActionType): IterableIterator<any> {
  let user: User = action.payload as User;
  try {
    const result: string = yield call(httpService.loginUserCredentials, user);
    if (result) {
      AsyncStorage.setItem("Token", result)
      yield put(loginUserCredentialsSuccess());
    }
  } catch (error) {
    yield put(loginUserCredentialsError(error));
  }
}

export function* fetchBooksHandler(action: AppActionType): IterableIterator<any> {
  try {
    const result: Book[] = yield call(httpService.fetchBooks);
    if (result) {
      yield put(fetchBooksSuccess(result));
    }
  } catch (error) {
    yield put(fetchBooksError(error));
  }
}

export function* addBookHandler(action: AppActionType): IterableIterator<any> {
  try {
    const result: Book = yield call(httpService.addNewBook, action.payload);
    if (result) {
      console.log(result)
      yield put(addBookSuccess(result));
    }
  } catch (error) {
    yield put(addBookError(error));
  }
}

export function* editBookHandler(action: AppActionType): IterableIterator<any> {
  try {
    const result: Book = yield call(httpService.editExistingBook, action.payload);
    if (result) {
      yield put(editBookSuccess(result));
    }
  } catch (error) {
    yield put(editBookError(error));
  }
}

export function* deleteBookHandler(action: AppActionType): IterableIterator<any> {
  try {
    const result: Book = yield call(httpService.deleteExistingBook, action.payload);
    if (result) {
      yield put(deleteBookSuccess(result));
    }
  } catch (error) {
    yield put(deleteBookError(error));
  }
}
