import User from "../../models/User";
import Book from "../../models/Book";

export const LOGIN_USER_CREDENTIALS_BEGIN = "LOGIN_USER_CREDENTIALS_BEGIN";
export const LOGIN_USER_CREDENTIALS_SUCCESS = "LOGIN_USER_CREDENTIALS_SUCCESS";
export const LOGIN_USER_CREDENTIALS_ERROR = "LOGIN_USER_CREDENTIALS_ERROR";

export const FETCH_BOOKS_BEGIN = "FETCH_BOOKS_BEGIN";
export const FETCH_BOOKS_SUCCESS = "FETCH_BOOKS_SUCCESS";
export const FETCH_BOOKS_ERROR = "FETCH_BOOKS_ERROR";

export const ADD_BOOK_BEGIN = "ADD_BOOK_BEGIN";
export const ADD_BOOK_SUCCESS = "ADD_BOOK_SUCCESS";
export const ADD_BOOK_ERROR = "ADD_BOOK_ERROR";

export const EDIT_BOOK_BEGIN = "EDIT_BOOK_BEGIN";
export const EDIT_BOOK_SUCCESS = "EDIT_BOOK_SUCCESS";
export const EDIT_BOOK_ERROR = "EDIT_BOOK_ERROR";

export const DELETE_BOOK_BEGIN = "DELETE_BOOK_BEGIN";
export const DELETE_BOOK_SUCCESS = "DELETE_BOOK_SUCCESS";
export const DELETE_BOOK_ERROR = "DELETE_BOOK_ERROR";

export const LOGOUT = "LOGOUT";

export interface LoginUserCredentialsBegin {
  type: typeof LOGIN_USER_CREDENTIALS_BEGIN;
  payload: User;
}
export interface LoginUserCredentialsSuccess {
  type: typeof LOGIN_USER_CREDENTIALS_SUCCESS;
  payload: undefined;
}
export interface LoginUserCredentialsError {
  type: typeof LOGIN_USER_CREDENTIALS_ERROR;
  payload: string;
}

export interface FetchBooksBegin {
  type: typeof FETCH_BOOKS_BEGIN;
  payload: undefined;
}

export interface FetchBooksSuccess {
  type: typeof FETCH_BOOKS_SUCCESS;
  payload: Book[];
}

export interface FetchBooksError {
  type: typeof FETCH_BOOKS_ERROR;
  payload: string;
}

export interface AddBookBegin {
  type: typeof ADD_BOOK_BEGIN;
  payload: Book;
}
export interface AddBookSuccess {
  type: typeof ADD_BOOK_SUCCESS;
  payload: Book;
}
export interface AddBookError {
  type: typeof ADD_BOOK_ERROR;
  payload: string;
}

export interface EditBookBegin {
  type: typeof EDIT_BOOK_BEGIN;
  payload: Book;
}
export interface EditBookSuccess {
  type: typeof EDIT_BOOK_SUCCESS;
  payload: Book;
}
export interface EditBookError {
  type: typeof EDIT_BOOK_ERROR;
  payload: string;
}

export interface DeleteBookBegin {
  type: typeof DELETE_BOOK_BEGIN;
  payload: Book;
}
export interface DeleteBookSuccess {
  type: typeof DELETE_BOOK_SUCCESS;
  payload: Book;
}
export interface DeleteBookError {
  type: typeof DELETE_BOOK_ERROR;
  payload: string;
}

export interface Logout {
  type: typeof LOGOUT;
  payload: undefined;
}

export type AppActionType =
  | LoginUserCredentialsBegin
  | LoginUserCredentialsSuccess
  | LoginUserCredentialsError
  | FetchBooksBegin
  | FetchBooksSuccess
  | FetchBooksError
  | AddBookBegin
  | AddBookSuccess
  | AddBookError
  | EditBookBegin
  | EditBookSuccess
  | EditBookError
  | DeleteBookBegin
  | DeleteBookSuccess
  | DeleteBookError
  | Logout;
