import {
  AppActionType,
  FETCH_BOOKS_BEGIN,
  FETCH_BOOKS_SUCCESS,
  FETCH_BOOKS_ERROR,
  LOGIN_USER_CREDENTIALS_BEGIN,
  LOGIN_USER_CREDENTIALS_SUCCESS,
  LOGIN_USER_CREDENTIALS_ERROR,
  DELETE_BOOK_BEGIN,
  DELETE_BOOK_SUCCESS,
  DELETE_BOOK_ERROR,
  EDIT_BOOK_BEGIN,
  EDIT_BOOK_SUCCESS,
  EDIT_BOOK_ERROR,
  ADD_BOOK_BEGIN,
  ADD_BOOK_SUCCESS,
  ADD_BOOK_ERROR,
  LOGOUT
} from "./types";
import Book from "../../models/Book";
import User from "../../models/User";

export function loginUserCredentialsBegin(user: User): AppActionType {
  return {
    type: LOGIN_USER_CREDENTIALS_BEGIN,
    payload: user
  };
}
export function loginUserCredentialsSuccess(): AppActionType {
  return {
    type: LOGIN_USER_CREDENTIALS_SUCCESS,
    payload: undefined
  };
}
export function loginUserCredentialsError(error: string): AppActionType {
  return {
    type: LOGIN_USER_CREDENTIALS_ERROR,
    payload: error
  };
}

export function fetchBooksBegin(): AppActionType {
  return {
    type: FETCH_BOOKS_BEGIN,
    payload: undefined
  };
}
export function fetchBooksSuccess(books: Book[]): AppActionType {
  return {
    type: FETCH_BOOKS_SUCCESS,
    payload: books
  };
}
export function fetchBooksError(error: string): AppActionType {
  return {
    type: FETCH_BOOKS_ERROR,
    payload: error
  };
}

export function addBookBegin(book: Book): AppActionType {
  return {
    type: ADD_BOOK_BEGIN,
    payload: book
  };
}
export function addBookSuccess(book: Book): AppActionType {
  return {
    type: ADD_BOOK_SUCCESS,
    payload: book
  };
}
export function addBookError(error: string): AppActionType {
  return {
    type: ADD_BOOK_ERROR,
    payload: error
  };
}

export function editBookBegin(newBook: Book): AppActionType {
  return {
    type: EDIT_BOOK_BEGIN,
    payload: newBook
  };
}
export function editBookSuccess(newBook: Book): AppActionType {
  return {
    type: EDIT_BOOK_SUCCESS,
    payload: newBook
  };
}
export function editBookError(error: string): AppActionType {
  return {
    type: EDIT_BOOK_ERROR,
    payload: error
  };
}

export function deleteBookBegin(book: Book): AppActionType {
  return {
    type: DELETE_BOOK_BEGIN,
    payload: book
  };
}
export function deleteBookSuccess(book: Book): AppActionType {
  return {
    type: DELETE_BOOK_SUCCESS,
    payload: book
  };
}
export function deleteBookError(error: string): AppActionType {
  return {
    type: DELETE_BOOK_ERROR,
    payload: error
  };
}

export function logoutUser(): AppActionType {
  return {
    type: LOGOUT,
    payload: undefined
  }
}