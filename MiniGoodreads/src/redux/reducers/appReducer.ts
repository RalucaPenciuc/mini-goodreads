import AppState from "../store/appStore";
import {
  fetchBooksBeginHandler,
  fetchBooksSuccessHandler,
  fetchBooksErrorHandler,
  loginUserBeginHandler,
  loginUserSuccessHandler,
  loginUserErrorHandler,
  deleteBookBeginHandler,
  deleteBookSuccessHandler,
  deleteBookErrorHandler,
  editBookBeginHandler,
  editBookSuccessHandler,
  editBookErrorHandler,
  addBookBeginHandler,
  addBookSuccessHandler,
  addBookErrorHandler,
  logoutHandler
} from "./appHandlers";
import {
  AppActionType,
  LOGIN_USER_CREDENTIALS_BEGIN,
  LOGIN_USER_CREDENTIALS_SUCCESS,
  LOGIN_USER_CREDENTIALS_ERROR,
  FETCH_BOOKS_BEGIN,
  FETCH_BOOKS_SUCCESS,
  FETCH_BOOKS_ERROR,
  DELETE_BOOK_BEGIN,
  DELETE_BOOK_SUCCESS,
  DELETE_BOOK_ERROR,
  EDIT_BOOK_BEGIN,
  EDIT_BOOK_SUCCESS,
  EDIT_BOOK_ERROR,
  ADD_BOOK_BEGIN,
  ADD_BOOK_SUCCESS,
  ADD_BOOK_ERROR,
  LOGOUT
} from "../actions/types";

const initialState: AppState = {
  loading: false,
  error: "",
  userLoggedIn: false,
  books: []
};

const appReducer = (state = initialState, action: AppActionType): AppState => {
  switch (action.type) {
    case LOGIN_USER_CREDENTIALS_BEGIN:
      return loginUserBeginHandler(state);
    case LOGIN_USER_CREDENTIALS_SUCCESS:
      return loginUserSuccessHandler(state);
    case LOGIN_USER_CREDENTIALS_ERROR:
      return loginUserErrorHandler(state, action.payload);
    case FETCH_BOOKS_BEGIN:
      return fetchBooksBeginHandler(state);
    case FETCH_BOOKS_SUCCESS:
      return fetchBooksSuccessHandler(state, action.payload);
    case FETCH_BOOKS_ERROR:
      return fetchBooksErrorHandler(state, action.payload);
    case EDIT_BOOK_BEGIN:
    case ADD_BOOK_BEGIN:
      return addBookBeginHandler(state);
    case ADD_BOOK_SUCCESS:
      return addBookSuccessHandler(state, action.payload);
    case ADD_BOOK_ERROR:
      return addBookErrorHandler(state, action.payload);
    case EDIT_BOOK_BEGIN:
      return editBookBeginHandler(state);
    case EDIT_BOOK_SUCCESS:
      return editBookSuccessHandler(state, action.payload);
    case EDIT_BOOK_ERROR:
      return editBookErrorHandler(state, action.payload);
    case DELETE_BOOK_BEGIN:
      return deleteBookBeginHandler(state);
    case DELETE_BOOK_SUCCESS:
      return deleteBookSuccessHandler(state, action.payload);
    case DELETE_BOOK_ERROR:
      return deleteBookErrorHandler(state, action.payload);
    case LOGOUT:
      return logoutHandler(state);
    default:
      return state;
  }
};

export default appReducer;
