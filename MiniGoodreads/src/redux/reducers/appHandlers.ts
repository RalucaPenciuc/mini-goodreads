import AppState from "../store/appStore";
import Book from "../../models/Book";
import { AsyncStorage } from "react-native";

export function loginUserBeginHandler(oldState: AppState): AppState {
  const newState = { ...oldState };
  newState.loading = true;
  newState.error = "";
  newState.userLoggedIn = false;
  return newState;
}

export function loginUserSuccessHandler(oldState: AppState): AppState {
  const newState = { ...oldState };
  newState.loading = false;
  newState.error = "";
  newState.userLoggedIn = true;
  return newState;
}

export function loginUserErrorHandler(oldState: AppState, error: string): AppState {
  const newState = { ...oldState };
  newState.loading = false;
  newState.error = error;
  newState.userLoggedIn = false;
  return newState;
}

export function fetchBooksBeginHandler(oldState: AppState) {
  const newState = { ...oldState };
  newState.loading = true;
  newState.error = "";
  newState.books = [];
  return newState;
}

export function fetchBooksSuccessHandler(oldState: AppState, books: Book[]): AppState {
  const newState = { ...oldState };
  newState.loading = false;
  newState.error = "";
  newState.books = books;
  return newState;
}

export function fetchBooksErrorHandler(oldState: AppState, error: string): AppState {
  const newState = { ...oldState };
  newState.loading = false;
  newState.error = error;
  newState.books = [];
  return newState;
}

export function addBookBeginHandler(oldState: AppState): AppState {
  const newState = { ...oldState };
  newState.loading = true;
  newState.error = "";
  return newState;
}

export function addBookSuccessHandler(oldState: AppState, book: Book): AppState {
  const newState = { ...oldState };
  newState.loading = false;
  newState.error = "";
  newState.books.push(book);
  return newState;
}

export function addBookErrorHandler(oldState: AppState, error: string): AppState {
  const newState = { ...oldState };
  newState.loading = false;
  newState.error = error;
  return newState;
}

export function editBookBeginHandler(oldState: AppState): AppState {
  const newState = { ...oldState };
  newState.loading = true;
  newState.error = "";
  return newState;
}

export function editBookSuccessHandler(oldState: AppState, newBook: Book): AppState {
  const newState = { ...oldState };
  newState.loading = false;
  newState.error = "";
  newState.books = oldState.books.filter((b: Book) => {
    return b._id !== newBook._id;
  });
  newState.books.push(newBook);
  return newState;
}

export function editBookErrorHandler(oldState: AppState, error: string): AppState {
  const newState = { ...oldState };
  newState.loading = false;
  newState.error = error;
  return newState;
}

export function deleteBookBeginHandler(oldState: AppState): AppState {
  const newState = { ...oldState };
  newState.loading = true;
  newState.error = "";
  return newState;
}

export function deleteBookSuccessHandler(oldState: AppState, book: Book): AppState {
  const newState = { ...oldState };
  newState.loading = false;
  newState.error = "";
  newState.books = oldState.books.filter((b: Book) => {
    return b._id !== book._id;
  });
  return newState;
}

export function deleteBookErrorHandler(oldState: AppState, error: string): AppState {
  const newState = { ...oldState };
  newState.loading = false;
  newState.error = error;
  return newState;
}

export function logoutHandler(oldState: AppState): AppState {
  const newState = {...oldState};
  newState.loading = true;
  newState.error = "";
  newState.userLoggedIn = false;
  newState.books = [];
  AsyncStorage.removeItem("Token");
  return newState;
}
