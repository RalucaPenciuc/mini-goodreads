import Book from "../../models/Book";

export default interface AppState {
  loading: boolean;
  error: string;
  userLoggedIn: boolean;
  books: Book[];
}
