import { createStore, Store, applyMiddleware, combineReducers } from "redux";
import AppState from "./appStore";
import { AppActionType } from "../actions/types";
import appReducer from "../reducers/appReducer";
import { createLogger } from "redux-logger";
import { generalSaga } from "../sagas/Saga";
import createSagaMiddleware, { SagaMiddleware } from "@redux-saga/core";
import { composeWithDevTools } from "redux-devtools-extension";
import { persistStore, persistReducer } from "redux-persist";
import { AsyncStorage } from "react-native";
import { rootReducer } from "../reducers";

const persistConfig = {
    key: "root",
    storage: AsyncStorage,
    blacklist: ['error', 'loading']
};
const persistedReducer = persistReducer(persistConfig, appReducer);
const sagaMiddleware: SagaMiddleware<AppState> = createSagaMiddleware();
const loggerMiddleware = createLogger();

export default function configureStore() {
  const store = createStore(
    persistedReducer,
    composeWithDevTools(applyMiddleware(sagaMiddleware, loggerMiddleware))
  );
  const persistor = persistStore(store);
  sagaMiddleware.run(generalSaga);
  return { persistor, store };
}
