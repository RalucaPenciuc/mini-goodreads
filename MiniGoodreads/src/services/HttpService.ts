import IHttpService from "./interfaces/IHttpService";
import { loginUrl, getBooksUrl, crudOperationBookUrl } from "./api";
import User from "../models/User";
import Book from "../models/Book";
import qs from "qs";
import { Linking } from "react-native";

export default class HttpService implements IHttpService {
  private static instance: HttpService;
  private constructor() {}
  static getInstance() {
    if (!HttpService.instance) HttpService.instance = new HttpService();
    return HttpService.instance;
  }

  async loginUserCredentials(user: User): Promise<string> {
    return await fetch(`${loginUrl}`, {
      method: "post",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(user)
    })
      .then(response => response.json())
      .then(response => {
        if (response["token"]) return response["token"];
        else if (response["issue"]) throw String(response["issue"][0]["error"]);
      })
      .catch(error => {
        throw String(error);
      });
  }

  async fetchBooks(): Promise<Book[]> {
    return await fetch(`${getBooksUrl}`, {
      method: "get",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => response.json())
      .then(response => {
        if (response["message"]) throw String(response["message"]);
        else return response;
      })
      .catch(error => {
        throw String(error);
      });
  }

  async addNewBook(book: Book): Promise<Book> {
    return await fetch(`${crudOperationBookUrl}`, {
      method: "post",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(book)
    })
      .then(response => response.json())
      .catch(error => {
        throw String(error);
      });
  }

  async editExistingBook(newBook: Book): Promise<Book> {
    return await fetch(`${crudOperationBookUrl}/${newBook._id}`, {
      method: "put",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(newBook)
    })
      .then(() => {
        return newBook;
      })
      .catch(error => {
        throw String(error);
      });
  }

  async deleteExistingBook(book: Book): Promise<Book> {
    return await fetch(`${crudOperationBookUrl}/${book._id}`, {
      method: "delete",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(() => {
        return book;
      })
      .catch(error => {
        throw String(error);
      });
  }
}

export async function sendEmail(to: string, subject: string, body: string) {
  let url: string = `mailto:${to}`;
  const query = qs.stringify({
    subject: subject,
    body: body
  });
  if (query.length) {
    url += `?${query}`;
  }

  const canOpen = await Linking.canOpenURL(url);
  if (!canOpen) {
    throw new Error("Provided URL can not be handled");
  }
  return Linking.openURL(url);
}
