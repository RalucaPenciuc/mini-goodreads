import User from "../models/User";
import HttpService from "./HttpService";

export default class Service {
  private static instance: Service;
  private constructor() {}

  static getInstance() {
    if (!Service.instance) Service.instance = new Service();
    return Service.instance;
  }

  // validateLoginUser(user: User): LoginException {
  //   let error: LoginException = {
  //     emailError: "",
  //     passwordError: ""
  //   };
  //   if (user.username.length === 0)
  //     error.emailError = "Email field cannot be empty!";

  //   if (user.password.length === 0)
  //     error.passwordError = "Password field cannot be empty!";

  //   return error;
  // }

  // validateValidationResult(result: LoginException): boolean {
  //   if (result.emailError.length > 0 || result.passwordError.length > 0)
  //     return true;
  //   return false;
  // }

  validateLoggedUser(user: User | undefined): boolean {
    if (user) {
      if (user.username.length > 0 && user.password.length > 0) return true;
    }
    return false;
  }
}
