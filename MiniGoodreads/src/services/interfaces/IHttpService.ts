import User from "../../models/User";
import Book from "../../models/Book";

export default interface IHttpService {
    loginUserCredentials(user: User): Promise<string>;
    fetchBooks(): Promise<Book[]>;
    addNewBook(book: Book): Promise<Book>;
    editExistingBook(newBook: Book): Promise<Book>;
    deleteExistingBook(book: Book): Promise<Book>;
}